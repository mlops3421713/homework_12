from datetime import timedelta
import os

from feast import (
    Entity,
    Field,
    FeatureView,
    FileSource,
)
from feast.types import Int64

ci_dir = os.environ.get("CI_PROJECT_DIR") or "."
feast_dir = os.environ.get("FEAST_DIR") or f"{ci_dir}/feast_repos/feature_repo"

observation = Entity(name="observation", join_keys=["observation_id"])

columns = [
    "elevation",
    "aspect",
    "slope",
    "horizontal_distance_to_hydrology",
    "vertical_distance_to_hydrology",
    "horizontal_distance_to_roadways",
    "hillshade_9am",
    "hillshade_noon",
    "hillshade_3pm",
    "horizontal_distance_to_fire_points",
    "wilderness_area_1",
    "wilderness_area_2",
    "wilderness_area_3",
    "wilderness_area_4",
    "soil_type_1",
    "soil_type_2",
    "soil_type_3",
    "soil_type_4",
    "soil_type_5",
    "soil_type_6",
    "soil_type_7",
    "soil_type_8",
    "soil_type_9",
    "soil_type_10",
    "soil_type_11",
    "soil_type_12",
    "soil_type_13",
    "soil_type_14",
    "soil_type_15",
    "soil_type_16",
    "soil_type_17",
    "soil_type_18",
    "soil_type_19",
    "soil_type_20",
    "soil_type_21",
    "soil_type_22",
    "soil_type_23",
    "soil_type_24",
    "soil_type_25",
    "soil_type_26",
    "soil_type_27",
    "soil_type_28",
    "soil_type_29",
    "soil_type_30",
    "soil_type_31",
    "soil_type_32",
    "soil_type_33",
    "soil_type_34",
    "soil_type_35",
    "soil_type_36",
    "soil_type_37",
    "soil_type_38",
    "soil_type_39",
    "soil_type_40",
    "cover_type",
]

train_source = FileSource(
    name="train_source",
    path=f"{feast_dir}/data/train.parquet",
    timestamp_field="event_timestamp",
)

test_source = FileSource(
    name="test_source",
    path=f"{feast_dir}/data/test.parquet",
    timestamp_field="event_timestamp",
)

train_fv = FeatureView(
    name="train_fv",
    entities=[observation],
    ttl=timedelta(days=1),
    schema=[Field(name=column, dtype=Int64) for column in columns],
    online=True,
    source=train_source,
)

test_fv = FeatureView(
    name="test_fv",
    entities=[observation],
    ttl=timedelta(days=1),
    schema=[Field(name=column, dtype=Int64) for column in columns],
    online=True,
    source=test_source,
)
