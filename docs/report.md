# Classification metrics

## Logistic Regression (standard scaler)

![Curves](report/metrics/lr_standard.png)
![Matrix](report/cmatrix/lr_standard.png)

## Logistic Regression (power transformer)

![Curves](report/metrics/lr_boxcox.png)
![Matrix](report/cmatrix/lr_boxcox.png)

## Naive Bayes (standard scaler)

![Curves](report/metrics/nb_standard.png)
![Matrix](report/cmatrix/nb_standard.png)

## Naive Bayes (power transformer)

![Curves](report/metrics/nb_boxcox.png)
![Matrix](report/cmatrix/nb_boxcox.png)

## Linear Discriminant (standard scaler)

![Curves](report/metrics/lda_standard.png)
![Matrix](report/cmatrix/lda_standard.png)

## Linear Discriminant (power transformer)

![Curves](report/metrics/lda_boxcox.png)
![Matrix](report/cmatrix/lda_boxcox.png)
