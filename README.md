# MLops homework: CD

This is a homework repository for the [MLOps & production for data science research 3.0](https://ods.ai/tracks/mlops3-course-spring-2024).

Based upon a Snakemake+Feast howework. A reproducible Snakemake pipeline. See docs/dag.md for the graph and description.

The report is also deployed at at https://homework-12-mlops3421713-7215de0d31ee92c5f509638e2a71caf1257206.gitlab.io/report/


Models are saved as artifacts (always have been).

There's no service using them per se, but a stub `deploy` stage was added to the CI pipeline, downloading the latest specific model from the registry. It can be theoretically pushed further (e.g. to the web service s3) or just used in-place.

## Versioning

Versioning is based on git branches. If you wanted a commit-based versioning, tough! I'm not doing that.

The `deploy` stage (manual for the demo purposes) requires a `MODEL_VERSION` variable to be defined, matching the git branch name (supposed to correspond to a product version). Current branches include `main` and `lda`.

This is as far as I wish to go with Gitlab storage. This is already way overengineered, sorry if that's not what you wanted to see.

## Running:

Expects a `$CI_PROJECT_DIR` (upper level) or `$FEAST_DIR` (directory containing `feature_store.yaml`) to be defined!

E.g.: `export CI_PROJECT_DIR=/home/user/sources/homework_12`

Create environment:

`conda env create -f feast.yaml`

Initialize feast if needed:

`cd feast_repos/feature_repo`

`feast apply`

`cd ../..`

Run the pipeline:

`conda run -n dev snakemake --cores all`
