FROM continuumio/miniconda3

WORKDIR /app

RUN apt-get update && apt-get install -y curl

# Creating environment
COPY feast.yaml .
RUN conda env create -f feast.yaml

# Copying project settings
COPY pyproject.toml .
