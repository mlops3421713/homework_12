from multiprocessing import cpu_count
from snakemake.report import auto_report


random_seed = 177013
test_size = 0.2
thread_count = lambda cores: max(1, cpu_count() // 2)

scaler_list = ['standard', 'boxcox']
model_list = ['lr', 'nb', 'lda']


rule all:
    input:
        metrics=expand('report/metrics/{model}_{scaler}.png', model=model_list, scaler=scaler_list),
        cmatrix=expand('report/cmatrix/{model}_{scaler}.png', model=model_list, scaler=scaler_list)

rule preprocess:
    output:
        train='processed/{scaler}/train.csv',
        test='processed/{scaler}/test.csv'
    params:
        random_seed=random_seed,
        test_size=test_size,
        scaler='{scaler}'
    threads:
        thread_count
    resources:
        mem_mb=64
    script:
        "preprocess.py"

rule train:
    input:
        train='processed/{scaler}/train.csv',
    output:
        model='processed/{scaler}/model_{model}.joblib',
        encoder='processed/{scaler}/encoder_{model}.joblib'
    params:
        random_seed=random_seed,
        model='{model}'
    threads:
        thread_count
    resources:
        mem_mb=64
    script:
        "train.py"

rule evaluate:
    input:
        test='processed/{scaler}/test.csv',
        model='processed/{scaler}/model_{model}.joblib',
        encoder='processed/{scaler}/encoder_{model}.joblib'
    output:
        metrics='report/metrics/{model}_{scaler}.png',
        cmatrix='report/cmatrix/{model}_{scaler}.png'
    threads:
        thread_count
    resources:
        mem_mb=64
    script:
        "evaluate.py"

onsuccess:
    with open("report/dag.txt","w") as f:
        f.writelines(str(workflow.persistence.dag))
    shell("cat report/dag.txt | dot -Tsvg > report/dag.svg")
